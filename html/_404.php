<section class="page page-404" data-name="{_module}" data-method="{_method}" data-pageid="{_pageid}">
  <div class="page-inner">
  	<div class="container">
	    <h1>404</h1>
	    <h3>Страница не найдена. <a href="/">На главную</a></h3>
  	</div>
  </div>
</section>