<?

$script = new StyleScriptCache('css',60,array(
	'tpl/css/_page_search.css',
	'tpl/css/_page_menu.css',
	'tpl/css/_page_map.css',
	'tpl/css/_popup_cart.css',
	'tpl/css/_popup_cart.css',
	'tpl/js/lib/scrollBar/jquery.tinyscrollbar.min.css',
));
$script = new StyleScriptCache('js',60,array(
'tpl/bootstrap/js/bootstrap.min.js',
'tpl/js/lib/scrollBar/jquery.tinyscrollbar.js',
'tpl/js/lib/dragdealer-master/src/dragdealer.js',
// 'tpl/js/lib/jquery.nanoscroller.min.js',
'tpl/js/functions.js',
'tpl/js/_config.js',
'tpl/js/_project.js',
'tpl/js/_scrollbar.js',
'tpl/js/_page_menu.js',
'tpl/js/_page_map.js',
'tpl/js/_page_index.js',
'tpl/js/_page_search.js',
'tpl/js/_cart.js',
'tpl/js/_dish.js',

'tpl/js/main.js',
));

?>

  <!--[if gt IE 9]> 
<script src="./tpl/ie/ie10-viewport-bug-workaround.js"></script> 
<![endif]--> 
   