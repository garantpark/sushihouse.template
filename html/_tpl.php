<!DOCTYPE html>
<html>
<head> 
	<meta charset="utf-8"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta name="description" content=""> 
  <meta name="keywords" content=""> 
  <title>Sushi House</title> 
  <?include('./html/_head.php');?> 
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>  
  </head> 
  <body>
    <?include('./html/_top.php');?>
    <?include('./html/_cart.php');?>

    <div id="pages">    
    <?include('./html/index.php');?>
    
    </div>    
    <?include('./html/_bottom.php');?>    
  </body>
</html>