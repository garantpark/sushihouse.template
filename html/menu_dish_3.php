<section class="page page-menu-dish page-set" data-name="menu" data-method="dish" data-pageid="{_pageid}">
	<a class="arrow arrow_left" href="#/menu/dish/id=2"><i class="sp sp-a1"><i></i></i></a>
	<a class="arrow arrow_right" href="#/menu/dish/id=1"><i class="sp sp-a2"><i></i></i></a>
	<div class="cloud"></div>

  <div class="page-inner">
  	<div class="container">

    <a class="arrow2 arrow_back" href="#/menu/map/id=3"><i class="sp sp-a3"><i></i></i> <span>К запеченным роллам</span></a>


	 		<div class="row">
  			<div class="col-md-6 col-sm-6 col-xs-12 photobg">
  				<div class="bigtmb">
	  				<span><img src="/img/samples/food1_1.png"></span>
	  				<span><img src="/img/samples/food1_2.png"></span>
	  				<span><img src="/img/samples/food1_3.png"></span>
	  			</div>
	  			<div class="smalltmb">
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_1.png"></a>
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_2.png"></a>
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_3.png"></a>
	  			</div>
  			</div>
  			<div class="col-md-6 col-sm-6 col-xs-12 description-outer">

  				<div class="description">
  					<div class="descr-inner">
  							<span class="label label-brown">Популярное</span>
  							<br>
  							<br>
  							<h1>Золотой сет</h1>
  							<div class="hr"></div>
  							<big>350 <i class="fa fa-rub"></i></big>
  							<p>
                    <small>Роллы:</small><br>
                    <strong>
                      <span>8 шт.</span>&mdash;&nbsp;Королевский <br>
                      <span>4 шт.</span>&mdash;&nbsp;Фуджияма <br>
                      <span>8шт.</span>&mdash;&nbsp;Лава <br>
                      <span>8шт.</span>&mdash;&nbsp;Император<br>
                    </strong>
                </p>
  							<div data-id="3" class="cartbox"></div>
  							<br>
  							<br>
  							<br>

                <strong>Состав сета</strong>
                <ul class="set-ingredients">
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                  <li>
                    <img src="/img/samples/food_search.png">
                    <small>Королевский ролл</small>
                    <span>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</span>
                  </li>
                </ul>
  				      
  					</div>
  				</div>


  			</div>
  		</div>
  	</div>
  </div>
</section>