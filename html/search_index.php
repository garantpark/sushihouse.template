<section class="page page-search dark1" data-name="{_module}" data-method="{_method}" data-pageid="{_pageid}">
  <div class="page-inner">
  		
  		<div class="search-top">
  		<div class="container">
  
		    <h3>Поиск по блюдам</h3>
		    <div class="search-field">
		    	<span class="text-suggestion" id="search-text-suggestion" data-default="Введите слово">Введите слово</span>
		    	<span class="controls">
		    		<input type="text" value="" onkeydown="sfDown(this,event)" onkeyup="sfSugg(this)">&nbsp;<a class="btn btn-border" href="javascript:void(0);" onclick="doSearch()"><i class="fa fa-search"></i>&nbsp;Найти</a>
		    	</span>
		    </div>
		    <div class="hr"></div>
		  </div>
		  </div>



	    <div class="search-results" id="search-results" onscroll="searchResScroll()">
	    	<div class="container">
  
	    	<div class="no-result">Введите слово для поиска</div>

	    </div>
  	</div>
  </div>
</section>
