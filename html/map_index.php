<div id="popup-map" class="popup-smoke">
    <!--  -->
  <div class="inner">
    <div class="smoke smoke01"></div>
    <div class="smoke smoke02"></div>

    <div class="container">

      <div class="heading">
        <a class="closer" href="#/map"><i class="sp sp-cart-x"><i></i></i></a>
        <i class="sp logo sp-6-logo-min"><i></i></i>
       </div>
      <!--  -->
      <h3></h3>
      <div class="row">
        <div class="img col-md-12 col-sm-12 col-xs-12">
          <img src="">
        </div>
        
        <div class="phone">
            <i class="sp sp-12"><i></i></i>
            <small>позвонить</small>
            <big></big>
        </div>
        <div class="time">
            <i class="sp sp-13"><i></i></i>
            <small>время работы</small>
            <big></big>
        </div>

        <div class="info col-md-12 col-sm-12 col-xs-12">
          <b>Меню</b>
          <div class="menu"></div>
          <div class="clearfix"></div>
          <br>
          <b>Оплата</b>
          <div class="payment"></div>
          <div class="clearfix"></div>
          <br>
        </div>
        
        <div class="map col-md-12 col-sm-12 col-xs-12">
           <div id="popup-map-map"></div>
        </div>
        

      </div>
      
      <!--  -->
      
     
    </div>
    </div>
  </div>



  <section class="page page-map" data-name="map" data-method="index" data-pageid="{_pageid}">   

	<div class="map-outer">
		<a href="#/map/[id=1]" class="p1 active"><span></span><b>на Сенчихина 1а</b></a>
		<a href="#/map/[id=2]" class="p2"><span>Десерты</span><b>на Сенчихина 1а</b></a>
		<a href="#/map/[id=3]" class="p3"><span>Супы</span><b>на Сенчихина 1а</b></a>
		<a href="#/map/[id=4]" class="p4"><span>Роллы</span><b>на Сенчихина 1а</b></a>
		<a href="#/map/[id=5]" class="p5"><span>Сеты</span><b>на Сенчихина 1а</b></a>
		<a href="#/map/[id=6]" class="p6"><span>Открытие нового суши-бара</span><b>на Сенчихина 1а</b></a>
	</div>


	<div class="page-inner">
  	<div class="container">
	    <h1>Рестораны<br>Суши Хаус в Улан-Удэ</h1>
	    <!-- <h3>Страница в разработке</h3> -->
  	</div>
  </div>

</section>