<section class="page page-shares" data-name="shares" data-method="index" data-pageid="{_pageid}">
  
	<div class="tinyScrollbar_outter">
	<div class="tinyScrollbar">
          <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
          <div class="viewport">
          <div class="overview">


<div class="page-inner">
  	<div class="container">

	    <h1>Акции «Суши хаус»</h1>
	  


	  	<div class="row">
	  		<div class="col-md-6 col-sm-6 col-xs-12 img-preview">
	  			<img src="/img/samples/action.png">
	  		</div>
	  		<div class="col-md-6 col-sm-6 col-xs-12">
	  			<h2>Заказ с собой со скидкой 10%</h2>
	  			<p>
	  				Вы можете сами забрать заказ из ближайшего кафе в удобное для вас время. Приготовление блюд начинается только после того, как вы сделали заказ. Время приготовления уточняйте у оператора. 
	  			</p>
	  			<p>
						Минимальная сумма такого заказа не ограничена. Скидка в размере 10 % является постоянной для услуги «самовывоз».
	  			</p>
	  		</div>
	  	</div>
	  	<div class="row">
	  		<div class="col-md-6 col-sm-6 col-xs-12 img-preview">
	  			<img src="/img/samples/action.png">
	  		</div>
	  		<div class="col-md-6 col-sm-6 col-xs-12">
	  			<h2>Скидка на 2е блюдо - 40%!</h2>
	  			<p>
	  				Вы можете сами забрать заказ из ближайшего кафе в удобное для вас время. Приготовление блюд начинается только после того, как вы сделали заказ. Время приготовления уточняйте у оператора. 
	  			</p>
	  			<p>
						Минимальная сумма такого заказа не ограничена. Скидка в размере 10 % является постоянной для услуги «самовывоз».
	  			</p>
	  		</div>
	  	</div>
	  	<div class="row">
	  		<div class="col-md-6 col-sm-6 col-xs-12 img-preview">
	  			<img src="/img/samples/action.png">
	  		</div>
	  		<div class="col-md-6 col-sm-6 col-xs-12">
	  			<h2>Приведи друга - получи роллы бесплатно!</h2>
	  			<p>
	  				Вы можете сами забрать заказ из ближайшего кафе в удобное для вас время. Приготовление блюд начинается только после того, как вы сделали заказ. Время приготовления уточняйте у оператора. 
	  			</p>
	  			<p>
						Минимальная сумма такого заказа не ограничена. Скидка в размере 10 % является постоянной для услуги «самовывоз».
	  			</p>
	  		</div>
	  	</div>


  	</div>
  	</div>
  	</div>
  	</div>
  	</div>

  </div>
</section>