<section class="page page-menu-dish dark" data-name="menu" data-method="dish" data-pageid="{_pageid}">
	<a class="arrow arrow_left" href="#/menu/dish/id=1"><i class="sp sp-a1"><i></i></i></a>
	<a class="arrow arrow_right" href="#/menu/dish/id=3"><i class="sp sp-a2"><i></i></i></a>
	<div class="cloud"></div>

  <div class="page-inner">
  	<div class="container">

    <a class="arrow2 arrow_back" href="#/menu/map/id=3"><i class="sp sp-a3"><i></i></i> <span>К запеченным роллам</span></a>


	 		<div class="row">
  			<div class="col-md-6 col-sm-6 col-xs-12 photobg">
  				<div class="bigtmb">
	  				<span><img src="/img/samples/food1_1.png"></span>
	  				<span><img src="/img/samples/food1_2.png"></span>
	  				<span><img src="/img/samples/food1_3.png"></span>
	  			</div>
	  			<div class="smalltmb">
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_1.png"></a>
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_2.png"></a>
	  				<a href="javascript:void(0);" onclick="tmbClick(this)"><img src="/img/samples/food1_3.png"></a>
	  			</div>
  			</div>
  			<div class="col-md-6 col-sm-6 col-xs-12">

  				<div class="description">
  					<div class="descr-inner">
  							<span class="label label-brown">Популярное</span>
  							<br>
  							<br>
  							<h1>Запеченная филадельфия в кунжуте</h1>
  							<div class="hr"></div>
  							<big>350 <i class="fa fa-rub"></i></big>
  							<p>салат с хрустящей кожей лосося и свежими овощами под соусом ясай, подается теплым.</p>
  							<div data-id="2" class="cartbox"></div>
  							<br>
  							<br>
  							<br>
  							<div class="hr2">
	  							<s></s>
	  							<span>Ингредиенты</span>	  							
  							</div>
  							<ul class="ingredients">
  								<li>	
  									<img src="/img/ingredients/fish.jpg">
  									<span>Лосось</span>
  								</li>
  								<li>	
  									<img src="/img/ingredients/fish.jpg">
  									<span>Икра</span>
  								</li>
  								<li>	
  									<img src="/img/ingredients/fish.jpg">
  									<span>Баклажан</span>
  								</li>
  								<li>	
  									<img src="/img/ingredients/fish.jpg">
  									<span>Огурец</span>
  								</li>
  								<li>	
  									<img src="/img/ingredients/fish.jpg">
  									<span>Угорь</span>
  								</li>
  							</ul>
  					</div>
  				</div>


  			</div>
  		</div>
  	</div>
  </div>
</section>