    <link rel="icon" href="/tpl/favicon.ico"> 
    <link href="/tpl/css/loader.css" rel="stylesheet"> 
    
    <?
        $script = new StyleScriptCache('css',60,array(
        'tpl/bootstrap/css/bootstrap.min.css',
        'tpl/font-awesome/css/font-awesome.min.css',
        'tpl/ie/ie10-viewport-bug-workaround.css',
        'tpl/css/fonts.css',
        'tpl/css/animations.css',
        'tpl/css/sprites.css',
        'tpl/css/tpl.css',
        'tpl/css/pages.css',
        // 'tpl/css/modals.css',
        ));
        
        // <link rel="stylesheet" media="screen and (orientation:portrait)" href="/tpl/css/portrait.css">
        // <link rel="stylesheet" media="screen and (orientation:landscape)" href="/tpl/css/landscape.css">

        
        $script = new StyleScriptCache('js',60,array(
        'tpl/js/lib/jquery.min.js',
        // 'tpl/js/lib/jquery-ui.min.js',
        // 'tpl/js/lib/jquery.transit.min.js',
        ));

    ?>
    
