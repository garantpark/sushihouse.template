
<div id="popup-cart-present" class="popup-smoke">

	<div class="inner">
		<div class="smoke smoke01"></div>
		<div class="smoke smoke02"></div>

		<div class="container">

			<div class="heading">
				<!-- <a class="closer" href="#" onclick="$P.cart.togglePresent()"><i class="sp sp-cart-x"><i></i></i></a> -->
				
				<strong id="present_text">
					<span data-count="1">Выберите ролл в подарок</span>
					<span data-count="2">Выберите 2 ролла в подарок</span>
					<span data-count="3">Выберите 3 ролла в подарок</span>
				</strong>

			</div>
			<!--  -->
			<hr>
			<!--  -->
			<div class="cart-present">
				
				<ul>				
					<li data-id="4">
						<img src="/img/samples/food1.png">
						Сливочный ролл
						<span>
							<button type="button" onclick="unsetPresent(this,6)" class="is-selected btn btn-default btn-xs"><i class="fa fa-check" aria-hidden="true"></i> Выбрано</button>
							<button type="button" onclick="setPresent(this,6)" class="can-select btn btn-border btn-xs">Выбрать</button>
						</span>
					</li>
					<li data-id="5">
						<img src="/img/samples/food2.png">
						Сливочный ролл
						<span>
							<button type="button" onclick="unsetPresent(this,5)" class="is-selected btn btn-default btn-xs"><i class="fa fa-check" aria-hidden="true"></i> Выбрано</button>
							<button type="button" onclick="setPresent(this,5)" class="can-select btn btn-border btn-xs">Выбрать  </button>
						</span>
					</li>
					<li data-id="6">
						<img src="/img/samples/food3.png">
						Сливочный ролл
						<span>
							<button type="button" onclick="unsetPresent(this,4)" class="is-selected btn btn-default btn-xs"><i class="fa fa-check" aria-hidden="true"></i> Выбрано</button>
							<button type="button" onclick="setPresent(this,4)" class="can-select btn btn-border btn-xs">Выбрать</button>
						</span>
					</li>
				</ul>
			

			</div>
			<!--  -->
			<hr>
			<!--  -->
				
				<div class="text-center">					
					<a class="btn btn-lg btn-success" href="javascript:setPresents()">Перейти в корзину</a>
				</div>
			
		</div>
	</div>

	
</div>




<div id="popup-cart" class="popup-smoke">
	<!--  -->
	<div class="popup-success">
		<div class="img"><i class="sp sp-6-logo-white"><i></i></i></div>
		<h2>Спасибо за заказ</h2>
		<h4>Наш менеджер свяжется с вами для уточнения заказа</h4>
		<br>
		<br>
		<big>69-36-36</big>
		<p>колл-центр</p>
	</div>
	<!--  -->	
	<div class="popup-order">
		 				
		 			<div class="tinyScrollbar cartScrollbar">
          <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
          <div class="viewport">
          		<div class="overview">
                 <h2>Адрес доставки</h2>
								<br>
		
            			<form method="post" action="index.php?cart_send">
										<div class="row">
											<div class="col-md-4">Имя</div>
											<div class="col-md-8"><input type="text" required class="form-control" name="" value=""></div>
											<div class="col-md-4">Телефон</div>
											<div class="col-md-8"><input type="text" required class="form-control" name="" value=""></div>
											<div class="col-md-4">Email</div>
											<div class="col-md-8"><input type="text" required class="form-control" name="" value=""></div>
										</div>
										<div class="row">
											<div class="col-md-4">Оплата</div>
											<div class="col-md-4 col-sm-8 col-xs-8">
													<input type="radio" name="payment_method" required id="payment_method_1" value="1" checked="checked"> <label for="payment_method_1">наличными</label>
											</div>
											<div class="col-md-4 col-sm-8 col-xs-8">
													<input type="radio" name="payment_method" required id="payment_method_2" value="2"> <label for="payment_method_2">по карте</label>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">Доставка</div>
											<div class="col-md-4 col-sm-8 col-xs-8">
													<input type="radio" name="delivery_method" required id="delivery_method_1" value="1" checked="checked"> <label for="delivery_method_1">по городу</label>
											</div>
											<div class="col-md-4 col-sm-8 col-xs-8">						
													<input type="radio" name="delivery_method" required id="delivery_method_2" value="2"> <label for="delivery_method_2">самовывоз</label>
											</div>
										</div>
										<div class="row">
											<!-- <div class="cleatfix"></div> -->
											<div class="col-md-4">Улица</div>
											<div class="col-md-8"><input type="text" required class="form-control" name="" value=""></div>
											<div class="col-md-4">Дом</div>
											<div class="col-md-2"><input type="text" required class="form-control" name="" value=""></div>
											<span class="clearfix"></span>
											<div class="col-md-4">Квартира</div>
											<div class="col-md-2"><input type="text" required class="form-control" name="" value=""></div>
											<span class="clearfix"></span>
											<div class="col-md-4">Комментарий</div>
											<div class="col-md-8"><textarea name="" rows="3" class="form-control"></textarea></div>
									</div>

											<div class="col-md-8 col-md-offset-4"><button type="submit" class="btn btn-primary btn-lg" >Заказать доставку</button></div>

										<!-- </div> -->
									</form>
				</div>
				</div>
				</div>

	</div>
	<!--  -->
	<div class="inner">
		<div class="smoke smoke01"></div>
		<div class="smoke smoke02"></div>

		<div class="container">

			<div class="heading">
				<a class="closer" href="#" onclick="cart()"><i class="sp sp-cart-x"><i></i></i></a>
				
				<span class="cart-cc">
				<i class="sp sp-5b"><i></i></i>&nbsp;
						<b class="cart">
							<!-- <i></i> -->
							<span class="_cartCount">0</span>
						</b>
				</span>

				<span class="cart-sum">
					Ваш заказ: <b><span class="_cartSum">0</span> руб</b>
				</span>

			</div>
			<!--  -->
			
			<!--  -->
							<div class="tinyScrollbar_outter">
							<div class="tinyScrollbar" data-maxheight="">
          <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
          <div class="viewport">
          <div class="overview">

			<div class="cart-list">
				





			</div>
			</div>
			</div>
			</div>
			</div>
			<!--  -->
			<hr>
			<!--  -->
			<div class="total">
			<hr>
				<div class="pull-right">
				<a class="btn2 btn btn-sm btn-border" disabled href="javascript:void(0);">Корзина пуста</a>
				<a class="btn1 btn btn-sm btn-primary" href="javascript:$P.cart.step1()">Оформить</a>
				</div>
				<small>Итого:</small>
				<br>
				<big><span class="_cartSum">0</span> <i class="fa fa-ruble"></i></big>

			</div>
		</div>
	</div>


</div>
