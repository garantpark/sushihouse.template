SushiHouse

- Full AJAX site (php, js, jquery)
- Direct load only index page, then JS router sending requests to server
- All php includes must be written in in index.php
- All server requests handeling by pages.php
  + Routing /#/menu/dish/id=3  to  function page_menu_dish(){ $id = $this->data['id']; ... in pages.php
- All input data is in $this->data (also check input class)
- pages.php has included template-machine (v0.1):
  + $this->template->view('html/search_no_result',array('word'=>$s));
  + <div class="no-result">������ �� ������� {word}</div>

