<?

class cTemplate{
		var $module;
        var $method;
		var $paramsString;
		var $data;
		function __construct($module,$method,$data){
			$this->module = $module;
            $this->method = $method ? $method : 'index';
			// $this->params = $method;
            if(!is_array($data)) $data = array();
			$this->data = $data;
            // s($data);
            $this->paramsString = array();
            foreach($data as $dk=>$dv) {
                if(!is_array($dv)) $this->paramsString []= "{$dk}-{$dv}";
            }
            $this->paramsString = implode('-', $this->paramsString);
		}

   function view($file,$data=array(),$return=false){
    
        if($data) $this->data = $data;

        foreach($this->data as $k=>$v) ${$k} = $v;

        ob_start();
        include"{$file}.php";
        $out = ob_get_contents();
        ob_end_clean();

        $replace = array(
        		'{_module}' => $this->module,
                '{_method}' => $this->method,
        		'{_pageid}' => "{$this->module}-{$this->method}".($this->paramsString?"-{$this->paramsString}":'')
        	);
        foreach($this->data as $k=>$v) $replace["{".$k."}"] = $v;
        
        
        $out = str_replace(array_keys($replace),array_values($replace),$out);

        if(!$return) print $out;
        else return $out;
        	
   }

}