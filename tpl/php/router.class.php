<?

  class cROUTER{

    public $request_module='index';
    public $request_method='index';
    public $data;
    
    
    function __construct(){

      if(isset($_SERVER['REDIRECT_QUERY_STRING'])){
        header("location: /#{$_SERVER['REDIRECT_QUERY_STRING']}");
      }
      
      

      
    }


    function make_route($t){
          
      $_GET_KEYS = array_keys($_GET);

      if(is_array($_GET) && count($_GET_KEYS)>0){
          
      

          $keys = explode('_',$_GET_KEYS[0]);
          // set main controller        
          $this->request_module = $keys[0];

          // set main controller        
          if(isset($keys[1]))$this->request_method = $keys[1];

          

          //unset first element
          $get = array_splice($_GET, 1);
          // s($get);

          //fill this->data with GET+POST
          foreach($get as $k) {if(isset($get[$k]))$this->data[$k] = $get[$k];}
          foreach(array_keys($_POST) as $k) $this->data[$k] = $_POST[$k];
          
       }

    }

    function load_page($t){

      // s($this);
      // s($_GET);
      if(is_array($_GET) && count($_GET)>0){
          //execute method
          $function = "page_{$this->request_module}";
          if($this->request_method) $function .= "_{$this->request_method}";
            // var_dump(method_exists($t, $function));
          if(!method_exists($t,$function)){
            $this->page_404();
            // die();
          }
          else {
            $t->$function();
          }
         
      }    
      else{

        $this->page_index();
      }  

    }

  }