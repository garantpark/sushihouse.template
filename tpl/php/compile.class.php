<?

class StyleScriptCache{
	
	 var $debugmode = false;
	// var $debugmode = true;

	function __construct($type,$livetime,$files){

		$hash = $this->get_name_hash($files);
		$this->check_livetime($livetime,$files);

		
		if(!file_exists("tpl/cache/$hash.$type") ||  (time()-filectime("tpl/cache/$hash.$type")) > $livetime) {
			// require_once 'tpl/php/packer.php';
			$data = $this->compile_files($files,$type);
			$fo = fopen("tpl/cache/$hash.$type", 'w');
			fputs($fo,$data);
			fclose($fo);
		}

// s($_SERVER['DOCUMENT_ROOT']);

		if($this->debugmode){
			foreach ($files as $file) {
					switch ($type) {
						case 'css':
							print "<link href=\"/$file\" rel=\"stylesheet\">\n ";
							break;
						
						default:
							print "<script src=\"/$file\" type=\"text/javascript\"></script>\n ";
							break;
					}
			}
		}
		else{
			switch ($type) {
				case 'css':
					print "<link href=\"/tpl/cache/$hash.css\" rel=\"stylesheet\">\n ";
					break;
				
				default:
					print "<script src=\"/tpl/cache/$hash.js\" type=\"text/javascript\"></script>\n ";
					break;
			}
		}

	}


	function check_livetime($livetime,$files){

		$delete = false;

		if(!file_exists('tpl/cache/livetime.d')) $delete = true;
		else{
			$time = intval(file_get_contents('tpl/cache/livetime.d'));
			if((time()-$time) > 3600) $delete = true;

		}


		if($delete){
			
			/*
			foreach($files as &$file){ 
				$file = explode('/', $file);
				$file = $file[count($file)-1];
			}*/

			$dir = glob('tpl/cache/*'); // get all file names
			foreach($dir as $file){ // iterate files
			    if(filetype($file)!='dir')unlink($file); // delete file
			}


			$f = fopen('tpl/cache/livetime.d','w');
			fputs($f,time());
			fclose($f);

		}

	}
	function get_name_hash($array){
		$data = '';
		foreach ($array as $v){
			if(!file_exists($v)) die("<script>alert('StyleScriptCache:: file not exists [$v]')</script>");
			$time = ''; 
			// $time = filectime($v); 
			$file = explode('/', $v);
			$file = $file[count($file)-1];
		
			$data .= $file.$time;
		}
		return md5($data);
	}
function compile_files($array,$type){




		$data = '';
		foreach ($array as $v){
			if(!file_exists($v)) die("StyleScriptCache:: file not exists <b>$v</b>");
			$file = explode('/', $v);
			$file = $file[count($file)-1];
			$data .= "\n/* BOF $file */\n";
			// $data .= file_get_contents($v);

			 $text = file_get_contents($v);
    

				// $text = preg_replace('!/\*.*?\*/!s', '', $text);
				// $text = preg_replace('/\n\s*\n/', "\n", $text);
	
			 if($type=="js"
 && !strpos($file, '.min.')
			 	){



$text = preg_replace('!/\*.*?\*/!s', '', $text);
$text = preg_replace('/\n\s*\n/', "\n", $text);


// error_reporting(E_ALL);
// $packer = new Tholu\Packer\Packer($text, 'Normal', true, false, false);
// $text = $packer->pack();
// echo $packed_js;


// $text = preg_replace('(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)', '', $text);
// $text = preg_replace('/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/', '', $text);
// $text = preg_replace('/\n\s*\n/', "\n", $text);


			}


				$data .= $text;

			// $data .= preg_replace(array($lineComment,$blockComment,$newline,$spaces), '', file_get_contents($v));
			// $data .= preg_replace($blockComment, '', file_get_contents($v));
			$data .= "\n/* EOF $file */\n\n";
		}
		return $data;
	}

}