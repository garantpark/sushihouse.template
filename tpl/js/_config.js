// CONFIGS (_C) -------------------------------------------------------------------------------------------
var isTouchDevice = 'ontouchstart' in document.documentElement;
var $C = {
	'isTouchDevice' : isTouchDevice,
	preloadImages : [
		//'/img/system/outdoor-map-v2.png'
	],
	pages : {
		// wheelScrollTries:3,
		// slidingTiming: 1050,
		// slidingReadyTiming: 1100,
		foo:0
	},
	log : {
		console:true,
		title:false,
		body:false,
		logTouch: false,
		timing:500
	},
	scrollBar : {
		enable:false,
		height:140,
		foo:0
	},
	pageMenu:{
		indexBlocks:{
			1:  ["0.25","0.143"],
			2:  ["0.5","0"],
			3:  ["0.75" ,"0.143"],
			4:  ["0.5","0.2857"],
			5:  ["0.25","0.429"],
			6:  ["0"   ,"0.571"],
			7:  ["0.25","0.7145"],
			8:  ["0.5" ,"0.5715"],
			9:  ["0.75","0.429"],
			10: ["1"   ,"0.5715"],
			11: ["0.75"   ,"0.7145"],
			12: ["0.75","1"],
			13: ["0.5" ,"0.857"],
			14: ["0.25","1"]
		}
	}
};



