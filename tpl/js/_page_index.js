
$P.pages.index = {
lastPoint: {x:50,y:50},
pageIndexCanvas:false,
pageIndexTimer:false,

createCanvas: function(parent, width, height) {
        var canvas = {};
        canvas.node = document.createElement('canvas');
        canvas.context = canvas.node.getContext('2d');
        canvas.node.width = width || 100;
        canvas.node.height = height || 100;
        canvas.isDrawing = true;
        parent.appendChild(canvas.node);
        return canvas;
    },

init: function() {
        var container = $('#page-logo-canvas').get(0);
        var width = +$(container).width();
        var height = +$(container).height();
        fillColor = "#f4f2e7";

        if($(container).hasClass('loaded')) return;

        $(container).removeClass('loaded'); 
        $(container).find('canvas').remove();
        $P.pages.index.lastPoint = {x:50,y:50};
         $("section.page-index").removeAttr('data-animation');
           

        this.pageIndexCanvas = this.createCanvas(container, width, height);
        var ctx = this.pageIndexCanvas.context;
        ctx.lineJoin ='butt';
        ctx.lineCap = 'butt';
        // ctx.miterLimit = 3;
        this.pageIndexCanvas.w = width;
        this.pageIndexCanvas.h = height;

        if($("section.page-index").attr('data-animation')!='done') $("section.page-index").attr('data-animation','busy');
        $('#page-logo-canvas canvas').show();

        $("section.page-index .page-logo").css({cursor:'pointer'}).off()/*.hover(function(){
            $(this).addClass('hover');
        },function(){
            $(this).removeClass('hover');
        }).find('#page-logo-canvas canvas').off()*/.click(function() {
            $P.pages.index.clearTimer();            
            location.href="#/menu";
            /*
            $('#page-logo-canvas').stop().fadeOut('fast',function(){        
                $P.pages.index.lastPoint = {x:50,y:50};
                $("section.page-index").removeAttr('data-animation');
                                    
                $P.pages.index.init();
            });*/
        });

        ctx.clearTo = function(fillColor) {
            ctx.fillStyle = fillColor;
            ctx.fillRect(0, 0, width, height);  
        };
        ctx.clearTo(fillColor || "#ddd");
        $(container).css({opacity:1});
  
        // this.pageIndexCanvas.node.onmousemove = function(e) {
        this.pageIndexCanvas.draw = function(x,y) {

            // var x = e.pageX - $(this).position().left - ($P.tmp.screenWidth*0.5 - 657/2);
            // var y = e.pageY - $(this).position().top - ($P.tmp.screenHeight*0.25);

            var radius = 70; 
            var fillColor = '#ff0000';
            ctx.globalCompositeOperation = 'destination-out';
        
            
            ctx.strokeStyle = fillColor;
            ctx.lineWidth = radius;
            
            ctx.moveTo($P.pages.index.lastPoint.x, $P.pages.index.lastPoint.y);
            ctx.lineTo(x, y);
            ctx.stroke();

            $P.pages.index.lastPoint.x = x;
            $P.pages.index.lastPoint.y = y;
        };


        setTimeout(function(){
            // _log($P.pages.index);
            if($P.pages.index.pageIndexTimer) clearInterval($P.pages.index.pageIndexTimer);
            var r = rand(0,1);
             r = 1;
            if(r==1){
                var dx = 1, dy = rand(10,15);
                var timerI=0;
                

                    $P.pages.index.pageIndexTimer = setInterval(function(){
                    var x = parseInt($P.pages.index.lastPoint.x);
                    var y = parseInt($P.pages.index.lastPoint.y);
                    var w = parseInt($P.pages.index.pageIndexCanvas.w);
                    var h = parseInt($P.pages.index.pageIndexCanvas.h);

                    dx2 = rand(-2,2);
                    // dy = rand(-5,5);

                    if(x+dx+dx2 > w-20  ) $P.pages.index.clearTimer();
                    if(y+dy < 50 || y+dy > h  ) {dy = -dy; dxx=dx<0?-1:1;  }
                    $P.pages.index.pageIndexCanvas.draw(x+dx+dx2,y+dy);
                    timerI++;
                    if(timerI>1000) $P.pages.index.clearTimer();
                },5);
            }
            else{
                var dy = 1, dx = rand(10,15);
                var timerI=0;
                 $P.pages.index.pageIndexTimer = setInterval(function(){
                    var x = parseInt($P.pages.index.lastPoint.x);
                    var y = parseInt($P.pages.index.lastPoint.y);
                    var w = parseInt($P.pages.index.pageIndexCanvas.w);
                    var h = parseInt($P.pages.index.pageIndexCanvas.h);

                    dy2 = rand(-1,0);
                    // dy = rand(-5,5);

                    if(y+dy+dy2 > h-20  ) $P.pages.index.clearTimer();
                    if(x+dx < 50 || x+dx > w  ) {dx = -dx;  }
                    $P.pages.index.pageIndexCanvas.draw(x+dx,y+dy+dy2);
                    timerI++;
                    if(timerI>1000) $P.pages.index.clearTimer();
                },5);
            }


         
        },1000);

        $(container).addClass('loaded').css('display','block');
    },

    clearTimer: function(){
        

                // if($P.pages.index.pageIndexCanvas) delete $P.pages.index.pageIndexCanvas;
                /*
                $("section.page-index").mousemove(function(e){
                    if($('section.page-index').hasClass('animating')) return;

                    x = Math.floor(e.pageX/($P.tmp.screenWidth/100));
                    y = Math.floor(e.pageY/($P.tmp.screenHeight/100));

                    x = (x<=50 ? -(50-x)/2 : x/2);
                    y = (y<=50 ? -(50-y)/2 : y/2);
                    if(x>0 && x<5) x=5;
                    if(x<0 && x>-5) x=-5;
                    if(y>0 && y<5) y=5;
                    if(y<0 && y>-5) y=-5;
                    
                    // y = (y<=50 ? -(1*(y*0.5 )) : (1*((y-50)*0.5 )));

                    // document.title = x+':'+y;
                    $('section.page-index').addClass('animating');
                    $('section.page-index .page-logo .logo').css("transform",'translate('+Math.floor(x/3)+'%,'+Math.floor(y/3)+'%)')
                    $('section.page-index .page-logo .circle.circle1').css("transform",'translate('+Math.floor(x/3)+'%,'+Math.floor(y/3)+'%)')
                    $('section.page-index .page-logo .circle.circle2').css("transform",'translate('+Math.floor(x/2)+'%,'+Math.floor(y/2)+'%)')
                    $('section.page-index .page-logo .circle.circle3').css("transform",'translate('+Math.floor(x/1)+'%,'+Math.floor(y/1)+'%)')
                    setTimeout(function(){ $('section.page-index').removeClass('animating'); },500);
                });
        */

                $("section.page-index").attr('data-animation','done');

   
                clearInterval($P.pages.index.pageIndexTimer);
            }

};