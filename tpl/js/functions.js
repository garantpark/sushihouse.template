
//function for scroll up
function scrollup(){$('body, html').animate({scrollTop: 0});}
function scrollto(t){$('body, html').animate({scrollTop: t});}



function _log(s){
	$P.log.add(s);	
}

function plural(number, one, two, five) {
    number = Math.abs(number);
    number %= 100;
    if (number >= 5 && number <= 20) {
        return five;
    }
    number %= 10;
    if (number == 1) {
        return one;
    }
    if (number >= 2 && number <= 4) {
        return two;
    }
    return five;
} 




function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}
arrayClean = function(array,deleteValue) {
    var a = [];
    var ind=1;
  for (var i = 0; i < array.length; i++) {
    if (array[i] == deleteValue || typeof array[i] == undef || !array[i]) {         
      array.splice(i, 1);
      i--;
    }
    else{
        a[ind] = array[i];
        ind++;
    }
  }

  // a.shift();


  return a;
};


$.fn.serializeObject=function(){var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};





    function swipedetect(el, callback){
  
    var touchsurface = el,
    swipedir,
    startX,
    startY,
    distX,
    distY,
    threshold = 500, //required min distance traveled to be considered swipe
    restraint = 1000, // maximum distance allowed at the same time in perpendicular direction
    allowedTime = 400, // maximum time allowed to travel that distance
    elapsedTime,
    startTime,
    handleswipe = callback || function(swipedir){}
  
    touchsurface.addEventListener('touchstart', function(e){
        // if($C.log.logTouch) _log('- swipedetect > start');
        var touchobj = e.changedTouches[0]
        swipedir = 'none'
        dist = 0
        startX = touchobj.pageX
        startY = touchobj.pageY
        startTime = new Date().getTime() // record time when finger first makes contact with surface
        // e.preventDefault()
    }, true)
  
    
    touchsurface.addEventListener('touchmove', function(e){
        var touchobj = e.changedTouches[0];
        distY = touchobj.pageY - startY;

        if(!$P.pages.pageObj.hasClass('page-listing') && ( ($P.pages.scrollState=='begin' && distY<0) || ($P.pages.scrollState=='end' && distY>0))) e.preventDefault() // prevent scrolling when inside DIV
    }, false)
  
    touchsurface.addEventListener('touchend', function(e){
        // if($C.log.logTouch) _log('- swipedetect > touchend');
        var touchobj = e.changedTouches[0];
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime; // get time elapsed
        if (elapsedTime <= allowedTime){ // first condition for awipe met
           /* if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
                swipedir = (distX < 0)? 'left' : 'right'; // if dist traveled is negative, it indicates left swipe
            }
            else */

            if (Math.abs(distY) >= threshold /*&& Math.abs(distX) <= restraint)*/){ // 2nd condition for vertical swipe met
                swipedir = (distY < 0)? 'up' : 'down'; // if dist traveled is negative, it indicates up swipe
            }
        }
        if($C.log.logTouch==true) _log("- swipedetect: "+swipedir+" [state: "+$P.pages.scrollState+"]");


        //detect if need to change section
        if($P.pages.pageObj.hasClass('page-listing') && ( ($P.pages.scrollState=='begin' && swipedir=='down') || ($P.pages.scrollState=='end' && swipedir=='up') )){    
            if(swipedir=='up') $P.pages.nextPage();
            else if(swipedir=='down') $P.pages.prevPage();
        }
        else if(!$P.pages.pageObj.hasClass('page-listing') && $P.pages.scrollState!='scrolling'){
            if(swipedir=='up') $P.pages.nextPage();
            else if(swipedir=='down') $P.pages.prevPage();

        }


        handleswipe(swipedir)
        if($P.pages.scrollState=='scrolling') e.preventDefault()
    }, true)
}


function rand(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}



function array_diff (arr1) { // eslint-disable-line camelcase
  //  discuss at: http://locutus.io/php/array_diff/
  // original by: Kevin van Zonneveld (http://kvz.io)
  // improved by: Sanjoy Roy
  //  revised by: Brett Zamir (http://brett-zamir.me)
  //   example 1: array_diff(['Kevin', 'van', 'Zonneveld'], ['van', 'Zonneveld'])
  //   returns 1: {0:'Kevin'}

  var retArr = {}
  var argl = arguments.length
  var k1 = ''
  var i = 1
  var k = ''
  var arr = {}

  arr1keys: for (k1 in arr1) { // eslint-disable-line no-labels
    for (i = 1; i < argl; i++) {
      arr = arguments[i]
      for (k in arr) {
        if (arr[k] === arr1[k1]) {
          // If it reaches here, it was found in at least one array, so try next value
          continue arr1keys // eslint-disable-line no-labels
        }
      }
      retArr[k1] = arr1[k1]
    }
  }

  return retArr
}



function objCount(a){
  var count = 0;
  var i;

  for (i in a) {
      if (a.hasOwnProperty(i)) {
          count++;
      }
  }

  return count;
}

function number_format (number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
  var n = !isFinite(+number) ? 0 : +number
  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
  var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
  var s = ''
  var toFixedFix = function (n, prec) {
    var k = Math.pow(10, prec)
    return '' + (Math.round(n * k) / k)
      .toFixed(prec)
  }
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    s[1] += new Array(prec - s[1].length + 1).join('0')
  }
  return s.join(dec)
}