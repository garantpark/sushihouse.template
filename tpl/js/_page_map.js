
$P.pages.map = {
	data:'',
	map:'',
	loading:false,

    resize:function(){
          // alert(2);
    if($P.tmp.screenWidth<1400){
      p = 1-(Math.floor((1500-$(window).width())/12) / 100);
      // alert(p);
      $('section.page-map .map-outer').css({'transform':'scale('+p+','+p+')'});
    }
    else 
      $('section.page-map .map-outer').css({'transform':''});


  },
  
	init:function(){




		if($('section.page-map').hasClass('inited')) return;
		$('head').append('<script src="https:'+'/'+'/api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');
		
		$.getJSON( "index.php?map_data", function(j) {
  		$P.pages.map.loading = false;
  		$P.pages.map.data = j;
  	});

  	$('section.page-map').addClass('inited');
	},
	setGet:function(){		
		// _log($P.GET);
		this.closePopup();

		if($P.GET['id']){
			this.toggle($P.GET['id']);
		}
	},
  unload: function(){
    if($P['pages']['map']['map'])
    	 $P.pages.map.map.destroy();
  },
  closePopup: function(){
  	 $('#popup-map').removeClass('success');
     $('#popup-map').addClass('out');
     setTimeout(function(){$('#popup-map').removeClass('out active');},1000);
  },
  toggle: function(id){

    if($('#popup-map').hasClass('in out')) return;
    var st = $('#popup-map').hasClass('active');
    if(!id || st){
     $P.pages.map.closePopup();
    }
    else{

    	j = $P.pages.map.data['map'+id];
    	var $m = $('#popup-map');
      $m.off().click(function(e){ if(e.target.id=='popup-map') $P.pages.map.closePopup();  });
    	$m.find('h3').text(j.title);
    	$m.find('.img img').attr('src',j.img);
    	$m.find('.phone big').text(j.phone);
    	$m.find('.time big').text(j.workingTime);
    	$m.find('.info .menu').text(j.menu);
    	$m.find('.info .payment').text(j.payment);


    	if(typeof ymaps !='undefined'){
	    	if($P.pages.map.map){
	    		 $P.pages.map.map.setCenter(j.coords[0],j.coords[1]);
	    	}
	    	else{
		    	$P.pages.map.map = new ymaps.Map("popup-map-map", {
		            center: j.coords, 
		            zoom: 13
		        });
		    	$P.pages.map.map.behaviors.disable('scrollZoom');
		    }
    	}

      $('#popup-map').addClass('in');
      setTimeout(function(){$('#popup-map').removeClass('in').addClass('active');},1000);

    }

  }


};