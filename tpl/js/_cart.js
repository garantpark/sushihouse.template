

$P.cart = {
	presentSum: 500,
	data:[],
	busy:false,
	mustHavePresents:0,
	presents:[],
	init:function(){
			

			$('.popup-order form').submit(function(){
				var cart={};
				for(var i in $P.cart.data){
					cart[i] = $P.cart.data[i].qty;
				}
				$.ajax({
					url:'index.php?cart_send',
					type: 'post',
					dataType: 'json',
					data:{"form":$('.popup-order form').serialize(),"cart": cart,"presents": $P.cart.presents},
					success: function(j){

						$P.cart.success();
					}
				});				
				return false;
			});
			
			if(localStorage.getItem("_cart")){
				this.data = array_diff(JSON.parse(localStorage.getItem("_cart")),[null]);
			}
			if(localStorage.getItem("_presents")){
				this.presents = localStorage.getItem("_presents").replace('[','').replace(']','').split(',');
			}

			$('.cartbox').removeClass('active');
			$('._cartCount').text(objCount($P.cart.data));

			for(var id in $P.cart.data){
				if($P.cart.data[id]==null) continue;
				$P.dish.load(id);
				$('.cartbox[data-id="'+id+'"]').addClass('active').find('b').text($P.cart.data[id].qty);
			}


			if(this.presents.length){
				for(var i in this.presents){
					$P.dish.load(this.presents[i]);
					$('.cart-present li[data-id="'+this.presents[i]+'"]').addClass('active');
				}
				act = $('#popup-cart-present .cart-present li.active').size();
				if(act>=$P.cart.mustHavePresents){
					$('#popup-cart-present .cart-present li:not(.active)').addClass('disabled');
				}
			}			

		
		
	},
	update:function(){
		  $('.cartbox').html('');
		  $('.cartbox').each(function(){
		    var s = '<a href="javascript:void(0);" class="min"><i class="sp sp-minus"><i></i></i></a>';
		        s += '<b>1</b>';
		        s += '<a href="javascript:void(0);" class="pls"><i class="sp sp-plus"><i></i></i></a>';
		        $(this).append(s);
		  });
		  $('.cartbox a').off().click(function(e){
		    e.preventDefault();
		    if($(this).hasClass('min')) $P.cart.del(e,this);
		    else $P.cart.add(e,this);
		    return false;
		  });
		  s = $('.cartScrollbar').data('plugin_tinyscrollbar');
		  s.update();
	},
	add: function(e,t){
		if($P.busy) return;
		$P.showLoader();

		if(parseInt(t)===t){
			var id = t;
		}
		else{
		  if(e && e['preventDefault'])e.preventDefault();
		  $(t).closest('.cartbox').addClass('active');
		  var id = $(t).closest('.cartbox').attr('data-id');
		}

		
		$P.dish.load(id);

	  if(this.data[id]){
	  	this.data[id].qty ++;
	  }
	  else{
	  	this.data[id] = {qty :1};	
	  }

	  // alert(this.data[id].qty);
	  $('.cartbox[data-id="'+id+'"] b').text(this.data[id].qty);
	  this.save();
	  this.updateBox();
	  $P.hideLoader();

	},
	save: function(e,t){
		this.data = array_diff(this.data,[null]);
		localStorage.setItem("_cart",JSON.stringify(this.data));
		if(this.presents.length) {
			localStorage.setItem("_presents",this.presents.join(','));
		}
		else{
			localStorage.removeItem("_presents");	
		}
	},
	del: function(e,t){
		if($P.busy) return;
		$P.showLoader();

		if(parseInt(t)===t){
			var id = t;
		}
		else{		
		  if(e && e['preventDefault'])e.preventDefault();	  
		  var id = $(t).closest('.cartbox').attr('data-id');
		}

	  if(this.data[id].qty>0){
	  		this.data[id].qty--;	  		
	  		if(this.data[id].qty==0) {

	  			delete this.data[id];
	  			$('.cartbox[data-id="'+id+'"]').removeClass('active').find('b').text(0);	  			
	  		}
	  		else
	  			$('.cartbox[data-id="'+id+'"]').find('b').text(this.data[id].qty);
	  }
	  else{
	  	delete this.data[id];
	  }
	  

	  this.save();
	  this.updateBox();
	  $P.hideLoader();



	},

	updateBox: function(){
		// if($P.busy) return;
		// $P.showLoader();

		$('.cart-list .presents').remove();
		// _log($P);

		$('._cartCount').text(objCount($P.cart.data));
		$('#popup-cart').attr('data-count',0);
		$('#popup-cart').attr('data-count',objCount($P.cart.data));

		var s='';		
		var sum=0;		

		if($P.cart.presents.length){
			s+='<div class="presents"><p><b>Подарки</b></p>';
			for(var i in $P.cart.presents){ 			
				d = $P.dish.get($P.cart.presents[i]);			
				
				s+='<div class="row cpRow-1"><div class="col-md-6 col-sm-12 col-xs-12 name">'+
				'<a href="/#/menu/dish/id='+$P.cart.presents[i]+'">'+d.title+'</a>'+
				'</div><div class="col-md-3 col-sm-6 col-xs-6 count">'+
				''+
				'<button type="button" onclick="$P.cart.togglePresent()" class="btn btn-cart">Изменить</button>'+
				
				'</div><div class="col-md-3 col-sm-6 col-xs-6 img"><img src="/img/cart/b1.png"></div></div>';
			}

			s+='<br><br></div>';
		}

		// $('._cartCount').text($P.cart.data.length);
		for(var id in $P.cart.data){ 			
			d = $P.dish.get(id);			
			sum+= d.price*$P.cart.data[id].qty;

			s+='<div class="row cpRow-1"><div class="col-md-6 col-sm-12 col-xs-12 name">'+
			'<a href="/#/menu/dish/id='+id+'">'+d.title+'</a>'+
			'<b>'+
			// number_format((d.price),0,'.',' ')+
			// " x "+
			// $P.cart.data[id].qty+" = "+
			number_format((d.price*$P.cart.data[id].qty),0,'.',' ')+
			' руб</b>'+
			'</div><div class="col-md-3 col-sm-6 col-xs-6 count">'+
			'<a href="javascript:void(0);" onclick="cpCartAdd('+id+')" class="plus"><i class="sp sp-cp"><i></i></i></a><b>'+$P.cart.data[id].qty+'</b>'+
			'<a href="javascript:void(0);" onclick="cpCartDel('+id+')" class="minus"><i class="sp sp-cm"><i></i></i></a></div><div class="col-md-3 col-sm-6 col-xs-6 img"><img src="/img/cart/b1.png">	</div></div>';


		}

		

		$('#popup-cart .cart-list').html(s);
		$P.cart.sum=sum;
		$P.cart.mustHavePresents = Math.floor(sum/$P.cart.presentSum);
		if($P.cart.mustHavePresents<1) $P.cart.mustHavePresents=0;
		if($P.cart.mustHavePresents>3) $P.cart.mustHavePresents=3;
		
		$('#popup-cart-present').attr('data-presents',$P.cart.mustHavePresents);


		$('._cartSum').html(number_format(sum,0,'.',' '));
		// $P.hideLoader();

		
	},
	success: function(){
		
		$P.cart.data = [];
		$P.cart.presents = [];
		localStorage.removeItem("_cart");	
		localStorage.removeItem("_presents");	
		$('.cartbox b').text('0');
		$('.cartbox.active').removeClass('active');

		$('#popup-cart').removeClass('order').toggleClass('success');
		// this.save();
	  this.updateBox();
	  // $P.hideLoader();
	  $P.cart.hide();

	},	
	order: function(){
		$('#popup-cart').removeClass('success').toggleClass('order');
	},
	hide: function(){
		$('#popup-cart').removeClass('success in out active');
		$('#popup-cart-present').removeClass('success in out active');
	},
	toggle: function(){
		if($('#popup-cart').hasClass('in out')) return;
		var st = $('#popup-cart').hasClass('active');
		if(st){
			$('#popup-cart').removeClass('success');
			$('#popup-cart').addClass('out');
			setTimeout(function(){$('#popup-cart').removeClass('out active');},1000);
		}
		else{
			$P.cart.hide();
			$P.cart.updatePopup();

			$('#popup-cart').addClass('in');
			setTimeout(function(){
				$('#popup-cart').removeClass('in').addClass('active');
				$P.scrollbar.init();
				// setTimeout(function(){ $P.scrollbar.init();},500);
			},200);

		}

	},
	updatePopup: function(){
	
	},

	step1: function(){
		if($P.cart.sum>=$P.cart.presentSum && $P.cart.presents.length!=$P.cart.mustHavePresents){
			$P.cart.togglePresent();
		}
		else{
			$P.cart.order();
		}
	},
	hidePresent: function(){

	},
	togglePresent: function(over,st2){
		if($('#popup-cart-present').hasClass('in out')) return;



		// $P.cart.hide();

		var st = $('#popup-cart-present').hasClass('active');
		if(over) st=st2;
		if(st){
			$('#popup-cart-present').removeClass('success');
			$('#popup-cart-present').addClass('out');
			setTimeout(function(){$('#popup-cart-present').removeClass('out active');},1000);
		}
		else{	
			$('#popup-cart-present .cart-present li.disabled').removeClass('disabled');
			this.updatePopup();
			$('#popup-cart-present').addClass('in');
			setTimeout(function(){$('#popup-cart-present').removeClass('in').addClass('active');},1000);

		}

	}

};





function setPresents(){
	if($P.cart.busy) return;
	$P.cart.busy = true;


	$('.cart-list .presents').remove();
	var arr = [];
	if($('#popup-cart-present .cart-present li.active').size()){
		$('#popup-cart-present .cart-present li.active').each(function(){
			arr.push($(this).data('id'));			
		});
		$P.cart.presents = arr;
		$P.cart.presents = $P.cart.presents.filter(function(item, pos) {
    	return $P.cart.presents.indexOf(item) == pos;
		});
	}


	$P.cart.togglePresent();
	if($('#popup-cart-present .cart-present li.active').size()==$P.cart.mustHavePresents) $P.cart.order();
	else $('#popup-cart').removeClass('success order');
	$P.cart.save();
	$P.cart.updateBox();
	$P.cart.busy = false;
}
function setPresent(b,id){

	if($P.busy) return;
	$P.showLoader();

	$('#popup-cart-present .cart-present li.disabled').removeClass('disabled');

	disablePresents();

	li = $(b).closest('li');
	$P.dish.get(li.data('id'));		

	li.toggleClass('active');

	$P.dish.load(li.data('id'));		

	act = $('#popup-cart-present .cart-present li.active').size();


	if(act>=$P.cart.mustHavePresents){
		$('#popup-cart-present .cart-present li:not(.active)').addClass('disabled');
	}

	$P.hideLoader();

}


function unsetPresent(b,id){

	if($P.busy) return;
	$P.showLoader();
	
	$('#popup-cart-present .cart-present li.disabled').removeClass('disabled');

/*
	if($P.cart.presents && $P.cart.presents.indexOf(id)>-1){
		i = $P.cart.presents.indexOf(id);
		delete $P.cart.presents[i];
	}*/

	$P.cart.presents = []; //array_diff($P.cart.presents,[null]);

	li = $(b).closest('li');
	li.toggleClass('active');

	$P.dish.load(li.data('id'));		

	$P.cart.save();
	$P.cart.updateBox();
	$P.hideLoader();

}


function disablePresents(){
	act = $('#popup-cart-present .cart-present li.active').size();

	if(act>=$P.cart.mustHavePresents){
		$('#popup-cart-present .cart-present li:not(.active)').addClass('disabled');
	}
}
function cart(){
    $P.cart.toggle();
}



function cartHide(){
    $P.cart.hide();
}



function cpCartAdd(id){
	$P.cart.add({},id);
	$P.cart.updateBox();
	$P.cart.updatePopup();
}

function cpCartDel(id){
	$P.cart.del({},id);
	$P.cart.updateBox();
	$P.cart.updatePopup();
	// _log($P.cart.data);
	// if(!$P.cart.data.items[id]) $('.row.cpRow-'+id).remove();
}