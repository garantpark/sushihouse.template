$P.pages.menu_index = {
  resize:function(){
          // alert(2);
    if($P.tmp.screenWidth<1400){
      p = 1-(Math.floor((1500-$(window).width())/12) / 100);
      // alert(p);
      $('section.page-menu-index .map-outer').css({'transform':'scale('+p+','+p+')'});
    }
    else 
      $('section.page-menu-index .map-outer').css({'transform':''});


  },
  init:function(){    



    $('.page-menu-index .map-outer>ul>li>a').off().click(function(e){
      if($(this).parent().find('ul').length==0) return;
      $('.page-menu-index .map-outer ul>li').not($(this).parent()).removeClass('active');
      $(this).parent().toggleClass('active');
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
  }
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------
$P.pages.menu_dish = {
  init: function(){
    // $('.page-menu-dish .bigtmb img:not(:first)').hide();
    $('.page-menu-dish .photobg').attr('data-index',1);
    $P.cart.update();
    $P.cart.init();

  
    // $('.page-menu-dish .smalltmb img:not(:first)').hide();
  }
};
$P.pages.menu_map = {

canvasMask: false,

unload: function(){
  $('#canvas-mask').removeClass('loaded');
  this.canvasMask = false;
},
init: function(){

  $P.cart.update();
  $P.cart.init();




  if(!$('#canvas-mask').hasClass('loaded')){

    $P.pages.menu_map.canvasMask = new Dragdealer('canvas-mask', {
      x: 0,
      y: 0,
      // css3: false,
      vertical: true,
      speed: 0.7,
      loose: true,
      // left:30,
      // top:50,  
      // steps:5,
      requestAnimationFrame: true,
      dragStartCallback: function(x,y){ 
        $('#canvas-mask').addClass('dragging');
      },
       dragStopCallback: function(x,y){
        $('#canvas-mask').removeClass('dragging');          
       },
       animationCallback:function(x,y){
         if (!this.groupCompare(this.offset.current, this.offset.prev)) {
            $('#canvas-mask').addClass('busy');  
          }
          else{
            $('#canvas-mask').removeClass('busy');  
          }
       }
    });

    // $('#canvas-mask .page').hover(function(){ if(!$('#canvas-mask .handle').hasClass('zoomOut')) return; $(this).addClass('hover')},function(e) {$(this).removeClass('hover')});


    var eachI=1;
    $('#canvas-mask-menu a').hide();
    // _log($P);
    if($P.GET['page']) $('#canvas-mask').attr('data-page',$P.GET['page']);

    $('#canvas-mask .page').each(function(){ 
      $(this).addClass('menu-type-'+eachI).addClass('p'+($(this).index()+1)); 

      $('#canvas-mask-menu a[data-page="'+($(this).index()+1)+'"]').show().text( ($(this).index()+1<10?'0':'')+($(this).index()+1)+" "+$(this).find('big').text() );

      eachI++; 
      if(eachI==5) eachI=1; 
    });
    // $('#canvas-mask-controls .zoom').click(function(e){
    //   e.preventDefault();
    //   $P.pages.menu_map.zoom(e);
    // });
    // $('#canvas-mask .page').click(function(e){      
    //   if(!$(this).parent().hasClass('zoomOut')) return;    
    //   // location.hash='#/menu/map/[page='+($(this).index()+1)+']';
    //   location.hash = $P.clearHref+'/[page='+($(this).index()+1)+']';

    //   $P.pages.menu_map.zoom();
    // });

      $('#canvas-mask').mousewheel(function(event) {
                
          p = parseInt($(this).attr('data-page'));
          if(!p)p=1;
          if(event.deltaY < 0){
            p++;
            if(p>$(this).find('.handle > .page').length) p = 1;
          }else{
            p--;
            if(p<1) p = $(this).find('.handle > .page').length;
          }
          // _log(p);
          location.hash = $P.clearHref+'/[page='+p+']';

      });
      $('#canvas-mask').addClass('loaded');
    this.canvasMask.setValue($C.pageMenu.indexBlocks[1][0],$C.pageMenu.indexBlocks[1][1],1);  
    this.resize();
   }//if 1st time 
    
 
},
//---------------------------------------------------------------------------------------------------------
setGet: function(_GET){   
  // _log(_GET);
    if(_GET.page){
      // _log(2);
      c = $C.pageMenu.indexBlocks[_GET.page];
      $('#canvas-mask').attr('data-page',_GET.page);
      // _log(c);
      this.canvasMask.setValue(c[0],c[1]);
      
      $('#canvas-mask-menu a').removeClass('active');
      $('#canvas-mask-menu a[href="'+location.hash+'"]').addClass('active');
    }
 },
//---------------------------------------------------------------------------------------------------------
zoom: function(e){ 
  // _log(z);
  // if(z==1) 
 if(e)e.preventDefault();
 // $($P.pages.menu.canvasMask.handle).data('beforezoom',$($P.pages.menu.canvasMask.handle).attr('style'));
 // $($P.pages.menu.canvasMask.handle).data('bzx',$P.pages.menu.canvasMask.getValue()[0]).data('bzy',$P.pages.menu.canvasMask.getValue()[1]);
 $(this.canvasMask.handle).toggleClass('zoomOut')//.attr('style','');


  if($(this.canvasMask.handle).hasClass('zoomOut')) {
    this.canvasMask.options.scale = 0.18;
    this.canvasMask.renderHandlePosition();
    this.canvasMask.disable();
    
    $('#canvas-mask-menu').slideUp();
    $('#canvas-mask-controls').slideUp();
  }
  else {
    // $P.currentHref = -1;
    // $($P.pages.menu.canvasMask.handle).attr('style',$($P.pages.menu.canvasMask.handle).data('beforezoom'));
    // $P.pages.menu.canvasMask.setValue($($P.pages.menu.canvasMask.handle).data('bzx'),$($P.pages.menu.canvasMask.handle).data('bzy'));  
    $('#canvas-mask-menu').slideDown();
    $('#canvas-mask-controls').slideDown();

    this.canvasMask.options.scale = 1;
    this.canvasMask.renderHandlePosition();
    this.canvasMask.enable();
    // $P.pages.menu.canvasMask.reflow();
  }

  
  
  // else     $('#canvas-mask-menu .handle').addClass('zoomOut');
},
//---------------------------------------------------------------------------------------------------------
resize: function(){ 
    $('#canvas-mask').css({width:$P.tmp.screenWidth,height: $P.tmp.screenHeight });
    $('#canvas-mask .handle').css({width:$P.tmp.screenWidth*5 , height: $P.tmp.screenHeight*4.5  });
    $('#canvas-mask .handle .page').css({width:$P.tmp.screenWidth,height: $P.tmp.screenHeight });
    if(this.canvasMask) this.canvasMask.reflow();
},

foo:0
};







function tmbClick(a){
  var i = $(a).index();
  i++;
  $(a).closest('.photobg').attr('data-index',i);
}
