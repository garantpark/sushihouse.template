$P.dish = {
	cache:[],
	load: function(id){
		if(!this.cache[id]) {
			// $.getJSON( "index.php?cart_dish&id="+id, function( ans ) {
			// 	$P.dish.cache[ans.id]	= ans;
			// });
			
			$.ajax({
				url:"index.php?cart_dish&id="+id,
				dataType:"json",
				async:false,
				success:function(ans){
					$P.dish.cache[ans.id]	= ans;
				},
			});

		}
	},
	get: function(id){
		return $P.dish.cache[id];
	},
	add: function(id){},
};