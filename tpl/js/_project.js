/*

navigator.geolocation.getCurrentPosition(function(position) {
 alert(position.coords.latitude + " "+ position.coords.longitude);
});

*/

var undef;

var $P = {	
	prevHref: '-1',
	currentHref: '-1',
	clearHref: '-1',
	pages: {},
	GET: [],
	controller: 'index',
	method: 'index',
	params: '',
	paramsString: '',
	pageClass: 'index-index',
};

// PROJECT (_P) -------------------------------------------------------------------------------------------
$P.preloadImgs = function(){	

	var images  = [
		'/img/menu/splatter.jpg',
		'/img/map/map.png',
		'/img/menu/menu.png',
		'/img/search/find-smoke-white.png',
		'/img/menu/menu.png',
		'/img/menu/splatter-dark.png'
	];
	var imagesObj = [];
	// _log(images);

	for(var i=0;i<images.length;i++){
		imagesObj[i] = new Image();
		// _log(images[i]);
		imagesObj[i].src = images[i];
		$(imagesObj[i]).load(function(){
			// _log(this.src+' > done');
		});
	}
};


$P.init = function(){
	$P.log.init();	
	$P.ajaxInit();

	$P.preloadImgs();

	$('body').attr('data-loading','start');
	_log("♦ [START]");	
	this.tStart = Date.now();

/*
		// keyboard
		function doc_keyUp(e) {
			if($P.pages.slideState!='ready') return;

		    if (e.keyCode == 27) $P.pages.closeAllPopups();
		    if (e.keyCode == 33 || e.keyCode == 37 ) $P.pages.prevPage();
		    if (e.keyCode == 34 || e.keyCode == 39 ) $P.pages.nextPage();
		    // if (e.keyCode == 38 && (this.scrollState==false || this.scrollState=='begin')) this.prevPage();
				// if (e.keyCode == 40 && (this.scrollState==false || this.scrollState=='end')) this.nextPage();
		    if (e.keyCode == 35) $P.pages.lastPage();
		    if (e.keyCode == 36) $P.pages.firstPage();
		}		
		document.addEventListener('keypress', doc_keyUp, false);

		*/
	



	_log("> LOAD > resourses");	
	this.rStart = Date.now();
};

//---------------------------------------------------------------------------------------------------------
$P.scrollbar = {resizeTimer:{}};
$P.scrollbar.init = function(){
	$('.tinyScrollbar:visible').each(function(){
		this.id = 'tinyScrollbar_'+Math.floor(Math.random()*1000000000);
		var h = parseInt($(this).parent().height())-0;
		if($P.tmp.screenHeight>0 && h>=$P.tmp.screenHeight) h = $P.tmp.screenHeight-0;

		$(this).find('.viewport').height( h );
		
		// $(this).tinyscrollbar();
		$('#'+this.id).tinyscrollbar();
	});
}

$P.scrollbar.update = function(){
		$('.tinyScrollbar').each(function(){
			var h = parseInt($(this).parent().height())-0;
			var thisId = "#"+this.id;
			if(h>=$P.tmp.screenHeight) h = $P.tmp.screenHeight-0;			
			$(this).find('.viewport').height(h);
			
			if($P.scrollbar.resizeTimer[thisId]) clearTimeout($P.scrollbar.resizeTimer[thisId]);

			$P.scrollbar.resizeTimer[thisId] = setTimeout(function(){

				var p = $(thisId).data("plugin_tinyscrollbar");
				if(p) p.update({trackSize:h});			
			},200);
		});
}

//---------------------------------------------------------------------------------------------------------
$P.load = function(){
	_log("- done, "+(Date.now() - this.rStart)+"ms");	
	// $P.tmp.screenWidth =  window.innerWidth;
	// $P.tmp.screenHeight = window.innerHeight;				
	$P.disableLinks();
	$P.animateSmoke();
	// $P.preloadImages();
	$P.cart.init();
	setTimeout(function(){ 
		$P.scrollbar.init();
		$P.cart.updateBox();
	},500);
  

	
	setInterval(function(){$P.resize()},500);
	$P.checkHashTimer();
};
//---------------------------------------------------------------------------------------------------------
$P.finishLoad = function(){	

	$('body').attr('data-loading','done');
	_log("■ [FINISH] load time "+(Date.now() - this.tStart)+"ms");	

};
//---------------------------------------------------------------------------------------------------------
$P.checkHashTimer = function(){
	setInterval(function(){
		
		var h = location.hash;
		var h2 = location.hash;
		if(h==$P.currentHref) return;

		//index
		if(h=='') {
			h = '#/';
			h2 = '#/';
			location.hash = h;
		}
		
		// alert(h);
		// alert(h2);
		$P.GET = [];
		if(h.indexOf('[')>-1 && h.indexOf(']')>-1){
			var p1 = h.indexOf('[');
			var p2 = h.indexOf(']');
			// _log('var json = '+h.substr(p1,p2)+';');
			h_ = replaceAll(h.substr(p1,p2),'=',':');
			h_ = h_.replace('[','{');
			h_ = h_.replace(']','}');

			// _log('var json = '+h2+';');
			eval('$P.GET = '+h_+';');
			
			h2 = h.substr(0,p1);
			if(h2.substr(h2.length-1,1)=='/') h2 = h2.substr(0,h2.length-1);
		}
		
		$P.clearHref = h2;
		if($P.clearHref != h2) $P.unloadPage();
		
		//page routing
		h2 = h2.split('/');
		if(h2[1]=='') h2[1] = 'index';
		if(!h2[2] || h2[2]=='') h2[2] = 'index';

		var controller = h2[1];
		var method = h2[2];

		// _log(h);
		// _log(h2);

		$P.currentHref = h;
		if($P.clearHref != h2.join('/')) $P.unloadPage();



		$P.controller = controller;
		$P.method = 'index';
		$P.params = {};
		$P.paramsString = [];
		$P.pageClass = '';
		if(method) $P.method = method;

		if(h2.length>2){
			var args = h2.splice(3);
			for(var i in args){
				if(!args[i]) continue;
				s = args[i].split('=');
				$P.params[s[0]] = s[1];
				$P.paramsString.push(args[i].replace("=",'-'));
			}
			$P.paramsString = $P.paramsString.join('-');
		}
		// _log($P);
		$P.pageClass = $P.controller+"-"+$P.method+($P.paramsString!=''?"-"+$P.paramsString:'');
		$P.setActive(controller,method);
		$P.setPage();
		$P.setGet();

		
	},150);
};
//---------------------------------------------------------------------------------------------------------
$P.unloadPage = function(controller,method){	
	
	if($P.pages[$P.controller+"_"+$P.method] && $P.pages[$P.controller+"_"+$P.method].unload){
		$P.pages[$P.controller+"_"+$P.method].unload();
	}
	else if($P.pages[$P.controller] && $P.pages[$P.controller].unload){
		$P.pages[$P.controller].unload();
	}

	$('section.page.page-unload').remove();
};
//---------------------------------------------------------------------------------------------------------
$P.setActive = function(controller,method){	
	// this.controller = controller;
	// this.method = method;
	$('.main-menu li,.main-menu li a').removeClass('active');
	// alert('.main-menu a[href|="#/'+this.controller+'"]');
	$('body').attr('data-controller',controller);
	$('body').attr('data-method',method);
	$('.main-menu a[href|="#/'+controller+'"]').addClass('active');
};
//---------------------------------------------------------------------------------------------------------
$P.setGet = function(){	
	var GET = $P.GET;
	// _log($P.controller+"_"+$P.method);
	// _log($P);

	if(GET && $P.pages[$P.controller+"_"+$P.method] && $P.pages[$P.controller+"_"+$P.method].setGet){
		$P.pages[$P.controller+"_"+$P.method].setGet(GET);
	}
	else if(GET && $P.pages[$P.controller] && $P.pages[$P.controller].setGet){
		$P.pages[$P.controller].setGet(GET);
	}
};
//---------------------------------------------------------------------------------------------------------
$P.setPage = function(){	
	this.showLoader();
	this.loadPage();
	// alert($P.controller+"_"+$P.method);
	if($P.pages[$P.controller+"_"+$P.method] && $P.pages[$P.controller+"_"+$P.method].init){
		$P.pages[$P.controller+"_"+$P.method].init();
	}
	else if($P.pages[$P.controller] && $P.pages[$P.controller].init){
		$P.pages[$P.controller].init();
	}

	$P.scrollbar.update();
	$P.hideLoader();

};
//---------------------------------------------------------------------------------------------------------
$P.getCurrentClass = function(){	
	var s = '';
	s += this.controller;
	s += "-"+this.method;
};
//---------------------------------------------------------------------------------------------------------
$P.loadPage = function(controller,method){	
	// _log(this.controller);
	// _log(this.method);
	// _log(this.pageClass);
	var sel = 'section.page[data-pageid="'+this.pageClass+'"]';
	if($(sel).length==0){
		// this.showLoader();
		var path = 'index.php?'+this.controller;
		if(this.method && this.method!='index') path+='_'+this.method;

		$.ajax({
			url: path,
			type: 'POST',
			async: false,
			data: this.params,
			success:function(ans,textStatus,xhr){

				$('#pages').append(ans);
				$(sel).css({top:0});
				$P.hideLoader();
			},
			error:function(xhr){
				$P.controller = '404';
				// $P.hideLoader();
			}
		});
		
	}


// _log($P);
	$('section.page').removeClass('active');
	// _log($('section.page[data-pageid="'+this.pageClass+'"]').length);
	$('section.page[data-pageid="'+this.pageClass+'"]').addClass('active');

	//toggle dark mode
  if($('section.page[data-pageid="'+this.pageClass+'"]').hasClass('dark')) $('body').attr('data-color','dark');
  else $('body').removeAttr('data-color');

  //has scroll
  $('body').removeClass('hasScroll');
  h1 = +$('section.page[data-pageid="'+this.pageClass+'"] .page-inner').height();
  if(h1 > $(window).height()) $('body').addClass('hasScroll');
  

  
};

//---------------------------------------------------------------------------------------------------------
$P.resize = function(){	
	
	var sw = window.innerWidth;
	var sh = window.innerHeight;
	
	if(sw==$P.tmp.screenWidth && sh==$P.tmp.screenHeight) return;
	$P.tmp.screenWidth = sw;
	$P.tmp.screenHeight = sh;				
	
	for(var i in $P.pages){
		if(typeof $P.pages[i].resize == 'function'){
			$P.pages[i].resize();
		}
	}

	$P.scrollbar.update();
	
	_log("> RESIZE "+$P.tmp.screenWidth+"x"+$P.tmp.screenHeight+"");
	
};

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
$P.disableLinks = function(){
	$('a[href="#"]:not(.disabledLink)').click(function(e){e.preventDefault();e.stopPropagation();$(this).addClass('disabledLink');return false;});
	
};
//---------------------------------------------------------------------------------------------------------
$P.animateSmoke = function(){
	$(".popup-smoke:not(.event)").off().click(function(e){
		// _log(e);
		if($(e.target).hasClass('popup-smoke'))
			cartHide();
	}).mousemove(function(e){
		if(!$(this).is(":visible")) return;
				$(this).addClass('event');
				x = Math.floor(e.pageX/($P.tmp.screenWidth/100));
      y = Math.floor(e.pageY/($P.tmp.screenHeight/100));

      	x = x - 50;
      	y = y - 50;

      	x = Math.floor(x/20)*20;
      	y = Math.floor(y/20)*20;

      $(this).find('div.smoke01').css("background-position",'0 '+Math.floor(y/4)+'px')
      $(this).find('div.smoke02').css("background-position",'0 '+Math.floor(y/2)+'px')

  });
};
//---------------------------------------------------------------------------------------------------------
$P.ajaxInit = function(){
	$.ajaxSetup({
		beforeSend: function (xhr){xhr.setRequestHeader("REQUEST_TYPE",'AJAX');},
		complete: function(){
			// alert(1);
			$P.disableLinks();
			$P.animateSmoke();
			$P.scrollbar.init();
		}
	});


},
//---------------------------------------------------------------------------------------------------------
$P.showLoader = function(){
		$P.busy = true;
		$('.page-loading,.cssload-thecube').removeClass('hidding');
		$('.cssload-thecube').removeClass('hidding');
		$('.page-loading').fadeIn();
		$('body').addClass('loading');
};
//---------------------------------------------------------------------------------------------------------
$P.hideLoader = function(){
		$P.busy = false;
		$('.page-loading').addClass('hidding');
		setTimeout(function(){$('body').removeClass('loading');},100);
};
//---------------------------------------------------------------------------------------------------------
$P.tmp = {
	screenWidth:0,
	screenHeight:0,
	foo:0
};
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
$P.log = {
	items:[],
	dafaultTitle:"",
	init:function(){
		this.defaultTitle=document.title;
		if($C.log.title) $P.log.timer();
	},
	timer:function(){
		setInterval(function(){
			if($P.log.items.length==0) {
				document.title=$P.log.defaultTitle;
				return;
			}		
			document.title = $P.log.items[0];
			$P.log.items.shift();
		},$C.log.timing);
	},
	add:function(s){
		this.items.push(s);
		if($C.log.console && window.console) console.log(s);
		if($C.log.body){
			if(!$('#log').size()){ $('body').prepend('<div class="log" id="log" style="overflow:auto;padding:10px;font-family:\'Lucida Console\',Monaco,monospace; position:fixed;z-index:99999;bottom:30px; left:30px; width:400px;height:300px;background-color:rgba(242,242,242,0.9); border:1px solid #262626; border-radius:2px; font-size:14px; line-height:18px;"></div>'); }
			$('#log').prepend(s+"<br>");
		}
	}
};