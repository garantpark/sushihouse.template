$P.pages.search_index = {
	setGet: function(GET){
		
	}
};

function srBig(b){
	$('#search-results .row .heading').css({position:'static',top:0});
	$('#search-results .row.active').removeClass('active');
	$(b).closest('.row').addClass('active');	
}




function doSearch(){
	var s  =$('.page-search .search-field input').val();
	
	$.ajax({
		url:'index.php?search_result',
		data:{'word':s},
		success:function(ans){
			// if(ans)
			$('#search-results .container').html(ans);
		}
	});

}


function sfDown(i,e){
	
	$(i).val($(i).val().toLowerCase() );

	if(e.keyCode==13){
		loadSuggest();
		doSearch();
	}
}

function loadSuggest(){
	if($('.page-search .search-field .text-suggestion').text()=='') return;
	$('.page-search .search-field input').val( $('.page-search .search-field .text-suggestion').text() );
}

function sfSugg(i){
	var v = $(i).val();

	$.ajax({
		url:'index.php?search_suggest',
		data:{'suggest':v},
		success:function(ans){
			// if(ans)
			$('#search-text-suggestion').text(ans);
		}
	});

}

function searchResScroll(){
	return;
	var b = $('#search-results .row.active .heading');
	if(b.size()==0) return;
	var at = parseInt(b.offset().top);
	var st = parseInt(b.attr('st'));
		
	if(!b.attr('defY')) {
		b.attr('defY',at);
		dt = at;
	}else{
		dt = b.attr('defY');
	}

	if(!st || parseInt($('#search-results').scrollTop())<st){				
		if(at<381) {
			if(!st) b.attr('st',$('#search-results').scrollTop());
			b.css({position:'fixed',top:382});
			
		}
		else{
			b.css({position:'static',top:0});
			b.removeAttr('st');	
		}
	}

}
