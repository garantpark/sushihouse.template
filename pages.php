<?

class Pages extends cROUTER{

    function __construct() {
        parent::__construct();   
        $this->input = new cInput();
        $this->cart = new cCart();

    }
   function init(){           
       $this->template = new cTemplate($this->request_module,$this->request_method,$this->data);         
    }
    /**/    
   function page_index(){            
      $out = $this->template->view('html/_tpl',array(
          // "_cartJs"=>$this->cart->printJs()
        ),true);

      // print $out;
      print trim(preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $out));
    }

    function page_404(){            
      $this->template->view('html/_404');
    }
    //------------------------------------------------
    
    //search
    function page_search_suggest(){
      $s =  $this->input->post('suggest'); 
      //do DB query here
      if(strpos($s, 'к')===0){
        die('калифорния ролл');
      }
      die('');
    }
    function page_search_result(){
      $s =  $this->input->post('word');
      if(strpos($s, 'к')===0){
        $this->template->view('html/search_result');
        die();
      }
      $this->template->view('html/search_no_result',array('word'=>$s));
    }   
    function page_search_index(){
      $this->template->view('html/search_index');
    }   


    //cart
    function page_cart_dish(){
      $id =   $this->input->post('id');
      
      print json_encode($this->cart->getItemInfo($id));

    }
    function page_cart_send(){
      
      print json_encode(array('error'=>0,'errorItems'=>array()) + $_POST);

    }   
    
    //shares
    function page_shares_index(){
      $this->template->view('html/share_01');
    }   
    
    //map
    function page_map_index(){
      $this->template->view('html/map_index');
    }   
    function page_map_data(){
      header('Content-Type: application/json');
      $this->template->view('html/map_data');
    }   
    
    //menu
    function page_menu_index(){
      $this->template->view('html/menu_index');
    }       
    function page_menu_map(){
      $this->template->view('html/menu_map');
    }   
    function page_menu_dish(){
      $id = $this->data['id'];
      /*! delete this !*/ if($id>3) $id=3; if($id<1) $id=1;
      $this->template->view('html/menu_dish'."_$id");
    }   

    //delivery
   function page_delivery_index(){
      $this->template->view('html/delivery');
    }   
   


}